#!/usr/bin/env python3
import logging
import sqlite3
import time
import os
logging.basicConfig(level=logging.DEBUG,
                    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s")
import sys
from telegram.bot import Bot
from settings import telegram_bot_token as token
from numpy import arange as range, linspace

logger = logging.getLogger(__file__)
dirname = os.path.dirname(os.path.realpath(__file__))
dbname = os.path.join(dirname, 'pinger.db3')
max_track_time = 60 * 60 * 1.5
temp_image_name = os.path.join(dirname, '{}_temp_image.png')
channel_to_post = 253432077
channel_to_post = '@apic_em_reachability_channel'

create_hosts = 'CREATE TABLE IF NOT EXISTS "hosts" ( `host` TEXT, `enabled` INTEGER DEFAULT 1, `errors_row` INTEGER DEFAULT 0 )'
create_pings = 'CREATE TABLE IF NOT EXISTS `pings` ( `measuerment_time` REAL, `host` TEXT, `loss` REAL, `min` REAL, `avg` REAL, `max` REAL )'
get_hosts = 'SELECT host FROM hosts WHERE enabled == 1'


def get_tracked_hosts():
    return [x[0] for x in sqlite3.connect(dbname).execute(get_hosts)]


def draw(devices, timeout, filename=temp_image_name):
    import matplotlib.pyplot as plt
    plt.figure(figsize=(16, 9), dpi=400)
    for name, avg in devices:
        x_coords = [x[0] for x in avg]
        y_coords = [x[1] for x in avg]
        plt.plot(x_coords, y_coords, label=name)
        for x in avg:
            loss = x[2] or 0
            if loss > 0:
                if loss < 100:
                    plt.annotate('{}%'.format(loss),
                                 (x[0], x[1] + timeout / 20), rotation=90)
                plt.scatter(x[0], x[1], color=(loss / 100, 0, 0))
    # draw 'zero' line
    ticks = plt.xticks()[0]
    right = int(max(ticks)) + 1
    left = int(min(ticks)) - 1
    plt.plot((left, right), (0, 0), alpha=0, color='black')
    plt.xticks(
        [int(x) for x in ticks] + [int(time.time())],
        [time.strftime(
            '%F %T', time.localtime(x)) for x in ticks],
        rotation=45
    )
    plt.plot((left, right), (timeout, timeout),
             linestyle="--", color="black", linewidth=3)
    plt.yticks(linspace(0, timeout, num=20))
    plt.plot([time.time(), time.time()], [0, timeout],
             linestyle='--', linewidth=2, color="gray")
    plt.axis('auto')
    plt.legend(loc='best')
    plt.grid(True, which='both')
    plt.minorticks_on()
    plt.gca().xaxis.grid(True, which='minor', linestyle='--')
    plt.gca().xaxis.grid(True, which='major', linestyle='-', color='#222222')
    plt.gca().yaxis.grid(False, which='minor')
    plt.gca().set_axisbelow(True)
    plt.gca().set_title('Response time')
    plt.tick_params(labelright=True)
    plt.savefig(filename)
    return filename


def get_data_to_draw(what):
    ret = {}

    max_ping_time = sqlite3.connect(dbname).execute(
        'SELECT MAX(avg) FROM pings WHERE measurement_time > ?', (time.time() - max_track_time, )).fetchall()[0][0]
    logger.debug(max_ping_time)
    query_result = sqlite3.connect(dbname).execute(
        'SELECT host, measurement_time, avg, loss FROM pings WHERE measurement_time > ?',
        (time.time() - max_track_time, )
    )
    for dev, tim, avg, loss in query_result:
        if not ret.get(dev):
            ret[dev] = []
        ret[dev].append((tim, (avg or 0) + max_ping_time *
                         (loss if loss != None else 100.0) * 2 / 100, loss))
    return [(x, y) for x, y in ret.items()], max_ping_time


def draw_and_send(what=get_tracked_hosts(), whom=channel_to_post):
    to_draw, max_ping_time = get_data_to_draw(what)
    filename = draw(to_draw, timeout=max_ping_time * 1.5,
                    filename=temp_image_name.format(whom))
    bot = Bot(token=token)
    bot.sendPhoto(chat_id=whom, photo=open(filename, 'br'))
    try:
        os.remove(filename)
    except FileNotFoundError:
        pass


def alarm(where):
    if not where:
        return
    bot = Bot(token=token)
    hosts = ', '.join(['`{}` (`{}` times in a row)'.format(
        x.get('host'), x.get('row')) for x in where])
    bot.sendMessage(chat_id=channel_to_post,
                    text='Could not reach hosts: ' + hosts, parse_mode='Markdown')
    logger.error(str(where))


def get_alerts():
    errors = sqlite3.connect(dbname).execute(
        'SELECT host, errors_row FROM hosts WHERE errors_row > 0')
    logger.debug(str(errors))
    return [{"host": x[0], "row":x[1]} for x in errors]


def save(host, loss, trip):
    logger.debug('saving {} {} {}'.format(host, loss, trip))
    connection = sqlite3.connect(dbname)
    connection.execute(
        """
        INSERT INTO pings
        (measurement_time, host, loss, min, avg, max)
        VALUES (?, ?, ?, ?, ?, ?)
        """,
        (time.time(), host, loss, trip.get('min'), trip.get('avg'), trip.get('max')))
    if not trip:
        connection.execute(
            """UPDATE hosts SET errors_row = errors_row + 1 WHERE host == ?""", (host,))
    else:
        connection.execute(
            """UPDATE hosts SET errors_row = 0 WHERE host == ?""", (host,))
    connection.commit()


def perform_ping(hosts=get_tracked_hosts()):
    from cisco import nxapi_call
    if not isinstance(hosts, list):
        hosts = [hosts]
    for host in hosts:
        result = nxapi_call(["ping {} vrf management".format(host)])
        if not result.get('result'):
            logger.error(
                'Nexus returned with an error?\n{}'.format(result))
            continue
        message = result.get('result').get('msg').split('\n')
        statistics = None
        loss, trip = None, {}
        for line in message:
            if line.startswith('---'):
                statistics = 1
            elif statistics == 1:
                # get percent number of packet loss
                # 5 packets transmitted, 5 packets received, 0.00% packet loss
                loss = line.split(',')[-1]
                logger.debug(loss)
                loss = float(loss.split(' ')[1].rstrip('%'))
                if loss < 100:
                    statistics = 2
                else:
                    break
                logger.debug("got loss {} from {}".format(loss, line))
            elif statistics == 2:
                # round-trip min/avg/max = 0.652/0.838/1.27 ms
                trip = [float(x) for x in line.split(' ')[-2].split('/')]
                trip = {'min': trip[0], 'avg': trip[1], 'max': trip[2]}
                break
        if not loss:
            logger.error(
                'Could not parse Nexus response:\n{}'.format(result))
        save(host, loss, trip)


def main(must_perform_ping=True, must_draw=True):
    if must_perform_ping:
        perform_ping()
        alerts = get_alerts()
        alarm(alerts)
    if must_draw:
        draw_and_send()
    if not (must_perform_ping or must_draw):
        print('-p to ping, -d to draw')

if __name__ == '__main__':
    main('-p' in sys.argv[1:], '-d' in sys.argv[1:])
