
"""Some constants used in this bot"""
nxapi_ip = 'ip addr of nxapi server. Currently, only one is supported'
apic_em_ip = 'ip addr of apic-em server. Currently, only one is supported'
nxapi_credentials = ('admin', 'C1sco12345')
apic_em_credentials = {'username': 'admin', 'password': 'C1sco12345'}

telegram_bot_token='Place your bot token here. Contact t.me/BotFather for more details'
db_filename = 'apic-em.db3'
telegram_default_admins = [208757525] #populating db after re-creation