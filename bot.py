#!/usr/bin/env python3
import logging
logging.basicConfig(level=logging.DEBUG,
                    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s")

# pip3 install telegram
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.ext.dispatcher import run_async
import sqlite3
import os
# pip3 install requests
import requests
import cisco
from cisco_output_parser import parse_nx_response
from pinger import draw_and_send
import json
import time
logger = logging.getLogger(__file__)

from settings import db_filename as db_file_name, telegram_bot_token as token, telegram_default_admins
dirname = os.path.dirname(os.path.realpath(__file__))
db_filename = os.path.join(dirname, db_file_name)

apic = cisco.apic_em()


class db_wrapper(object):
    """This class was intended to encapsulate database working"""

    def __init__(self, filename=db_filename, init=False):
        self.filename = filename
        if init == False:
            self.init_db()

    def init_db(self, drop=False):
        create_statement = """
        CREATE TABLE IF NOT EXISTS "dialogs" (
            `_id`   INTEGER PRIMARY KEY NOT NULL
            , `rights`  INTEGER DEFAULT 0
            , `state`   INTEGER DEFAULT 0
            , `state_args` TEXT DEFAULT "{}"
            , `prefs` TEXT
        )
        """
        insert_statement = """
            INSERT OR IGNORE
            INTO "dialogs" (`_id`, `rights`)
            VALUES (?, ?)
        """
        connection = sqlite3.connect(self.filename)
        if drop is True:
            connection.execute('DROP TABLE IF EXISTS "dialogs"')

        connection.execute(create_statement)
        connection.executemany(
            insert_statement, [(x, 2047) for x in telegram_default_admins])
        connection.commit()

    def get_field(self, user, field):
        return sqlite3.connect(self.filename).execute('SELECT `{}` FROM "dialogs" WHERE `_id` == ?'.format(field), (user,)).fetchall()[0][0]

    def get_state(self, user):
        return self.get_field(user, "state")

    def reset_state(self, user):
        self.set_state(user, 0)

    def set_state(self, user, state):
        connection = sqlite3.connect(self.filename)
        try:
            connection.execute(
                'UPDATE "dialogs" SET `state` = ? WHERE `_id` == ?', (state, user))
            connection.commit()
        except:
            pass

    def get_permissions(self, user):
        return self.get_field(user, "rights")

    def load_arg(self, user):
        return json.loads(self.get_field(user, 'state_args'))

    def save_arg(self, user, arg):
        connection = sqlite3.connect(self.filename)
        connection.execute(
            'UPDATE "dialogs" SET `state_args` = ? WHERE `_id` == ?', (json.dumps(arg), user))
        connection.commit()

    def reset_arg(self, user):
        self.save_arg(user, {})

    def new_user(self, user):
        connection = sqlite3.connect(self.filename)
        connection.execute(
            'INSERT OR IGNORE INTO "dialogs" (`_id`) VALUES (?)', (user,))
        connection.commit()

db = db_wrapper(init=True)


class permission_handler():

    def __init__(self, database_wrapper):
        self.__db = database_wrapper

    def can_be_granted(self, desired, real):
        # strip '0b'
        a = [int(x) for x in bin(real)[2:]]
        b = [int(x) for x in bin(desired)[2:]]
        # python strips non-meaning zeroes. Return them here
        la = len(a)
        lb = len(b)
        a = [0] * (lb - la) + a
        b = [0] * (la - lb) + b
        # now they have same size. Zip and test
        zipped = zip(a, b)
        return all(p >= m for p, m in zipped)

    def has_access(self, user, action, perms=None):
        mask = action
        if not isinstance(action, int):
            mask = self.get_permission_integer(action).result()
        try:
            perms = perms or self.__db.get_permissions(user)
        except:
            # could not find user?
            logger.exception(
                'Error while trying to find perms of user {}'.format(user))
            perms = 0
        return self.can_be_granted(mask, perms)


perm = permission_handler(db)


def splitMessage(inmessage, max_size=4090, pairs=[[' ', ' '], ['<a', 'a>'], ['<pre>', '</pre>'], ['<code>', '</code>'], ['\n', '\n']]):
    """ a lazy approach to split message by words or tags """
    message = str(inmessage) + ' '
    splited = []
    start, end = 0, 0
    while start < len(message) - 1:
        end = start + max_size
        if end > len(message):
            end = len(message)
        for pair in pairs:
            pair_0 = message.rfind(pair[0], start, end)
            if pair_0 > -1:
                pair_1 = message.find(pair[1], pair_0 + 1, end)
            else:
                continue
            if pair_1 < 0:
                if end < len(message):
                    end = pair_0
        if end > start:
            splited.append(message[start:end])
        start = end
    return splited


@run_async
def sendMessage(bot, **kwargs):
    full_text = kwargs['text']
    kwargs['text'] = None
    kwargs['timeout'] = 60
    for text in splitMessage(full_text):
        kwargs['text'] = text
        bot.sendMessage(**kwargs)


def calculate_help(uid):
    ret = []
    security = []
    user_perms = db.get_permissions(uid)
    for handler in command_handlers:
        if perm.has_access(uid, handler['perms'], user_perms):
            ret.append('/{} -- {}'.format(handler['name'], handler['msg']))
        else:
            security.append(handler['name'])
    return "\n".join(ret)


def flow_analysis(bot, update, text=None):
    uid = update.message.from_user.id
    saved = db.load_arg(uid)
    args = ((text[0] if text and (len(text) > 0) else None)
            or get_command_args(update.message.text)).split(' ')
    if 'arr' in saved:
        args = saved['arr'] + args
    args = [x for x in args if x]
    if len(args) > 1:
        result = apic.request(
            "flow-analysis", {"sourceIP": args[0], "destIP": args[1]})
        flow_id = result['response']['flowAnalysisId']
        sendMessage(bot, chat_id=update.message.chat_id,
                    text="Waiting for flow analysis id = {}".format(flow_id))
        retries = 0
        while True:
            time.sleep(retries)
            result = apic.request(
                "flow-analysis/{}".format(flow_id))
            if result['response']['request']['status'] == 'COMPLETED':
                sendMessage(bot, chat_id=update.message.chat_id,
                            text=json.dumps(result['response'], indent=2))
                break
            retries += 1
        db.reset_arg(uid)
        return True
    else:
        db.save_arg(uid, {"arr": args})


def return_help(bot, update, accidentially=False):
    sendMessage(bot, chat_id=update.message.chat_id,
                text=calculate_help(update.message.from_user.id))
    return True


def get_command_args(text):
    return ' '.join(text.split(' ')[1:])


def reset_state(bot, update, help_needed=True):
    db.reset_state(update.message.from_user.id)
    if (update.message.chat.type == "private") and help_needed:
        sendMessage(bot, chat_id=update.message.chat_id,
                    text="You are in the main menu now. Any /help?")


def nxapi_command(bot, update, new_text=None):
    uid = update.message.from_user.id
    text = new_text[0] if new_text else None or get_command_args(
        update.message.text)
    if text:
        text = text.split('\n')
        unparsed_response = cisco.nxapi_call(text)
        try:
            parsed = parse_nx_response(unparsed_response)
        except:
            logger.exception(
                'parse_nx_response throwed on {}'.format(unparsed_response))
            parsed = unparsed_response
        sendMessage(bot, chat_id=update.message.chat_id,
                    text=parsed, parse_mode='HTML')
    return (update.message.chat.type != "private") and text


def stateful_message(bot, update):
    user_id = update.message.from_user.id
    text = update.message.text.split('\n')
    handler = get_handler_by_state(db.get_state(user_id))
    if handler is not None:
        handler['func'](bot, update, (update.message.text, True))


def unknown_command(bot, update):
    if update.message.chat.type == "private":
        sendMessage(bot, chat_id=update.message.chat_id,
                    text="Sorry, this command is unknown. /help?")


def get_handler_by_state(state):
    for handler in command_handlers:
        if handler['state'] == state:
            return handler


def start_handler(bot, update):
    uid = update.message.from_user.id
    db.new_user(uid)
    return_help(bot, update)


def ip_geo(bot, update, text=None):
    uid = update.message.from_user.id
    args = ((text[0] if text and (len(text) > 0) else None)
            or get_command_args(update.message.text))
    if args:
        result = apic.request(
            "ipgeo/{}".format(args).replace(' ', '%20'))
        sendMessage(bot, chat_id=update.message.chat_id,
                    text=json.dumps(result['response'], indent=2))
        for key, ip in result['response'].items():
            if 'latitude' in ip and "longitude" in ip:
                bot.sendLocation(chat_id=update.message.chat_id, latitude=ip[
                                 'latitude'], longitude=ip['longitude'], disable_notification=True)


def reachability_info(bot, update, text=None):
    uid = update.message.from_user.id
    result = apic.request("reachability-info")
    sendMessage(bot, chat_id=update.message.chat_id,
                text=json.dumps(result['response'], indent=2))
    return True


def check_reachability(bot, update, text=None):
    uid = update.message.from_user.id
    args = ((text[0] if text and (len(text) > 0) else None)
            or get_command_args(update.message.text))
    if args:
        result = apic.request(
            "reachability-info/ip-address/{}".format(args))
        sendMessage(bot, chat_id=update.message.chat_id,
                    text=json.dumps(result['response'], indent=2))
    return args


def draw_pings(bot, update, _=None):
    draw_and_send(whom=update.message.chat_id)
    return True

command_handlers = [
    {"name": "nxapi", "state": 1024, "perms": 1024,
        "msg": "use NXAPI of example controller", "func": nxapi_command, "on_enter": "Now send commands as you will send them to the switch (in order)\nOne message -- one context.\nYou will return to the main menu after first command sequence",
        "on_enter_private": "Now send commands as you will send them to the switch (in order)\nOne message -- one context.\nPress /cancel to return to main menu"},
    {"name": "help", "state": 0, "perms": 0,
        "msg": "get help", "func": return_help},
    {"name": "cancel", "state": 0, "perms": 0,
        "msg": "cancel current action", "func": reset_state},
    {"name": "flow_analysis", "state": 512, "perms": 512,
        "msg": "do flow-analysis", "func": flow_analysis, "on_enter": "Enter source ip and destination ip"},
    {"name": "ipgeo", "state": 256, "perms": 256, "msg": "get geoip",
        "func": ip_geo, "on_enter": "Enter WAN IP to query geo-coords"},
    {"name": "reachability", "state": 128, "perms": 128, "msg": "get reachability-info for all devices",
     "func": reachability_info},
    {"name": "check_reachability", "state": 64, "perms": 64, "msg": "get reachability-info for a device by ip",
     "func": check_reachability, "on_enter": "Enter IP of device to check"},
    {"name": "get_pings", "state": 0, "perms": 32, "msg": "get ping times for tracked devices",
     "func": draw_pings},
]


def report_security_incident(handler, bot, update):
    # This incident will be reported
    sendMessage(bot, chat_id=253432077, text="!!security!!")
    for part in splitMessage(update):
        sendMessage(bot, chat_id=253432077,
                    text='<pre>{}</pre>'.format(part))
    pass


def permissed_call(handler, bot, update):
    uid = update.message.from_user.id
    if(perm.has_access(uid, handler['perms'])):
        just_entered = False
        if db.get_state(uid) != handler['state']:
            db.reset_arg(uid)
            just_entered = True
        complete = handler['func'](bot, update)
        if complete:
            reset_state(bot, update, just_entered)
        else:
            if just_entered:
                db.set_state(uid, handler['state'])
            if 'on_enter_{}'.format(update.message.chat.type) in handler:
                sendMessage(bot, chat_id=update.message.chat_id,
                            text=handler['on_enter_{}'.format(update.message.chat.type)])
            elif 'on_enter' in handler:
                sendMessage(bot, chat_id=update.message.chat_id,
                            text=handler['on_enter'])
    else:
        logger.debug('no access')
        report_security_incident(handler, bot, update)


def error_handler(bot, update, tgerr):
    if 253432077 != update.message.chat_id:
        sendMessage(bot, chat_id=update.message.chat_id,
                    text='//sorry, could not complete your request because of internal error')
    sendMessage(bot, chat_id=253432077, text="!!error handler fired!!")
    for part in splitMessage(update) + splitMessage(tgerr):
        sendMessage(bot, chat_id=253432077,
                    text='<pre>{}</pre>'.format(part))


def main():
    up = Updater(token)
    dispatcher = up.dispatcher
    for handler in command_handlers:
        dispatcher.add_handler(CommandHandler(
            handler['name'], lambda x, y, h=handler: permissed_call(h, x, y)))

    dispatcher.add_handler(CommandHandler("start", start_handler))
    dispatcher.add_handler(MessageHandler(Filters.text, stateful_message))
    dispatcher.add_handler(MessageHandler(Filters.command, unknown_command))
    dispatcher.add_error_handler(error_handler)

    up.start_polling()

if __name__ == '__main__':
    main()
