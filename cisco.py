#!/usr/bin/env python3
import requests
import json
import logging
import time
from settings import nxapi_ip, nxapi_credentials, apic_em_ip, apic_em_credentials

try:
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
except:
    pass

logger = logging.getLogger(__file__)


class apic_em(object):
    """Work with apic-em API, deal with tickets renewal"""
    __header = {}
    __ticket = {
        'serviceTicket': '',
        'idleTimeout': 0,
        'idleSince': 0,
        'expiresAt': 0
    }

    def __init__(self, addr=apic_em_ip, credentials=apic_em_credentials):
        self.__credentials = json.dumps(credentials)
        self.__addr = addr
        self.__update_ticket(True)

    def __url(self, method, version="v1"):
        return 'https://{addr}/api/{version}/{method}'.format(addr=self.__addr, version=version, method=method)

    def __post(self, method, payload):
        url = self.__url(method)
        logger.debug(payload)
        response = requests.post(
            url, data=payload, headers=self.__header, verify=False)
        logger.debug(response.text)
        try:
            x = response.json()
            return x
        except:
            logger.error(response.text)
            raise

    def __get(self, method, params={}):
        url = self.__url(method)
        response = requests.get(
            url, headers=self.__header, params=params, verify=False)
        logger.debug(response.text)
        try:
            x = response.json()
            return x
        except:
            logger.error(response.text)
            raise

    def __is_ticket_valid(self):
        current_time = time.time()
        time_reserve = 5
        return (
            time_reserve + current_time - self.__ticket['idleSince']
            < self.__ticket['idleTimeout']
        ) and (
            time_reserve + current_time
            < self.__ticket['expiresAt']
        )

    def __update_ticket(self, force=False):
        if force == True or not self.__is_ticket_valid():
            logger.info(
                'Ticket {} was invalidated, updating..'.format(self.__ticket))
            self.__header = {"content-type": "application/json"}
            ticket = self.__post('ticket', self.__credentials)
            self.__header['X-Auth-Token'] = ticket['response']['serviceTicket']

            to_save = ticket['response']
            self.__ticket = {
                'serviceTicket': to_save['serviceTicket'],
                'idleTimeout': to_save['idleTimeout'],
                'idleSince': time.time(),
                'expiresAt': to_save['sessionTimeout'] + time.time()
            }
            logger.info('Success! The new ticket is {}'.format(self.__ticket))

    def request(self, method, payload=None, type='POST'):
        self.__update_ticket()
        self.__ticket['idleSince'] = time.time()
        if type == 'GET' or payload is None:
            return self.__get(method, payload)
        else:
            if isinstance(payload, dict):
                payload = json.dumps(payload)
            return self.__post(method, payload)


def nxapi_call(procedures, ip=nxapi_ip, credentials=nxapi_credentials):
    """Get the result of running a list of commands in Nexus"""
    url = 'http://{}/ins'.format(ip)
    myheaders = {'content-type': 'application/json-rpc'}
    payload = [
        {"jsonrpc": "2.0", "method": "cli", "params": {
            "cmd": cmd, "version": 1}, "id": idx}
        for idx, cmd in enumerate(procedures, start=1)]
    response = requests.post(url, data=json.dumps(
        payload), headers=myheaders, auth=credentials).json()
    return response
