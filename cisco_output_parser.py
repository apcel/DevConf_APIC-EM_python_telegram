import json


def dumps(*args, **kwargs):
    kwargs['indent'] = 2
    return json.dumps(*args, **kwargs)


def parse_nx_response(json_to_parse, **kwargs):
    ret = []

    if not isinstance(json_to_parse, list):
        json_to_parse = [json_to_parse]

    def check_table(js):
        return any(key.startswith('TABLE') for key in js.keys())

    def process_table(js, indent=0):
        def save_indented(*args, indent=indent):
            save('|' * indent, *args)

        def extract_name(tn, extract):
            return tn[len(extract):]

        def row_loop(row, indent):
            for key in row.keys():
                if not key.startswith('ROW'):
                    save_indented(key + ':', row[key])
                else:
                    save_indented(extract_name(key, 'ROW_'))
                    process_table(row[key], indent=indent + 1)

        def array_loop(arr):
            for idx, item in enumerate(arr):
                save_indented('Item {}:'.format(idx))
                process_table(item, indent=indent + 2)
        # table name
        if not isinstance(js, dict):
            return save_indented(js)
        for key in js.keys():
            if isinstance(js[key], list):
                return array_loop(js[key])
            if not key.startswith('TABLE'):
                save_indented(key + ':', js[key])
            else:
                # save_indented('table ', extract_name(key, 'TABLE_'))
                row_loop(js[key], indent=indent)

    def save(*args):
        ret.append(' '.join([str(x) for x in args]))
    for l in json_to_parse:
        if 'result' in l:
            if l['result'] == None:
                save('Nothing to output. Perhaps, this command returns nothing')
            elif 'msg' in l['result']:
                save(l['result']['msg'].replace('\\n', '\n'))
            elif 'header_str' in l['result']['body']:
                for key, value in l['result']['body'].items():
                    save(key + ':', value)
            # elif 'TABLE_vlanbrief' in l['result']['body']:
            #     save('VLAN Brief:')
            #     for key, value in l['result']['body']['TABLE_vlanbrief']['ROW_vlanbrief'].items():
            #         save(key + ':', value)
            #     save('MTU Info:')
            #     for key, value in l['result']['body']['TABLE_mtuinfo']['ROW_mtuinfo'].items():
            #         save(key + ':', value)
            # elif 'TABLE_interface' in l['result']['body']:
            #     save('Interface Brief:')
            #     for interface in l['result']['body']['TABLE_interface']['ROW_interface']:
            #         for key, value in interface.items():
            #             save(key + ':', value)
            #         save()
            elif check_table(l['result']['body']):
                process_table(l['result']['body'])
                # for key in l['result']['body'].keys():
                #     save(dumps({key: l['result']['body'][key]}))
            elif 'body' in l['result']:
                for key, value in l['result']['body'].items():
                    save(key + ':', value)
            else:
                process_table(l['result'])
        elif 'error' in l:
            try:
                save('<code>ERROR:</code>', l['error']['data']['msg'])
            except KeyError:
                save('<code>ERROR:</code>', l['error']['message'])
        else:
            process_table(l)
    return '\n'.join(ret)
